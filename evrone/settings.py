""" settings """
from pydantic import BaseModel


class Settings(BaseModel):
    """    create model user """
    database_url: str = 'sqlite:///./database.sqlite7'

    jwt_secret: str = '4eead6848a81134cd6bac1a00ca747ac0110cdeab26ad7bf1b46256d231631fd'
    jwt_algorithm: str = 'HS256'
    jwt_expiration: int = 3600


settings = Settings()
