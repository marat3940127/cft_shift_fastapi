""" settings """
from fastapi import FastAPI

from .api import router


app = FastAPI()
app.include_router(router)


@app.get("/")
def pytest():
    """ pytest """
    return {"msg": "Hello World"}
