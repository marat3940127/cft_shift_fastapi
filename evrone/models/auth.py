"""    create model user """
from pydantic import BaseModel


class BaseUser(BaseModel):
    """    create model user """
    email: str
    username: str
    salary: int


class UserCreate(BaseUser):
    """    create model user """
    password: str


class User(BaseUser):
    """    create model user """
    id: int

    class ConfigDict:
        """    create model user """
        from_attributes = True


class Token(BaseModel):
    """    create model user """
    access_token: str
    token_type: str = 'bearer'
