""" settings """
import sqlalchemy as sa
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, DeclarativeBase

from evrone.settings import settings

engine = create_engine(
    settings.database_url,
    connect_args={'check_same_thread': False},
)


class Base(DeclarativeBase):
    """    create model user """


class User(Base):
    """    create model user """
    __tablename__ = 'users'

    id = sa.Column(sa.Integer, primary_key=True)
    email = sa.Column(sa.Text, unique=True)
    username = sa.Column(sa.Text, unique=True)
    password_hash = sa.Column(sa.Text)
    salary = sa.Column(sa.Integer, unique=True)


Session = sessionmaker(
    engine,
    autocommit=False,
    autoflush=False,
)


def get_session() -> Session:
    """    create model user """
    session = Session()
    try:
        yield session
    finally:
        session.close()
