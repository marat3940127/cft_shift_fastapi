""" settings """
from fastapi.testclient import TestClient
from evrone.main import app


client = TestClient(app)


def test_pytest():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"msg": "Hello World"}
